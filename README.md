#Author

@Sayatbek
ISTEP Solutions
Almaty, Kazakhstan
2018

#Clone the source code

Go to:
https://bitbucket.org/sayatbek/carpart

or run this bash command:

```
$ git clone https://sayatbek@bitbucket.org/sayatbek/carpart.git
```

# Hippo CMS WebSite: CarPart.kz

This is the website of a CarPart company. This company was founded by Orazbekov brothers in 2017 and
it's business is to sell the high quality car parts from approved officials. First official shop is 
situated in Kenzhekhan, Almaty. 5th row and 28th boutique.

## Running locally

This project uses the Maven Cargo plugin to run Essentials, the CMS and site
locally in Tomcat.
From the project root folder, execute:

    mvn clean verify
    mvn -P cargo.run -D repo.path=storage

Access the applications at the following URLs:

* Hippo Essentials: `http://localhost:8080/essentials`
* Hippo CMS: `http://localhost:8080/cms`
* Website: `http://localhost:8080/`

Logs are located in `target/tomcat8x/logs`.

## Best Practice for development

Use the option `-Drepo.path=/some/path/to/repository` or simple `storage` during start up. This
will avoid your repository to be cleared when you do a `mvn clean`.

For example start your project with:

    mvn -P cargo.run -Drepo.path=/home/usr/tmp/repo
    
## Automatic Export

Automatic export of repository changes to the file system is turned on by
default. To control this behavior, log into `http://localhost:8080/cms/console`
and press the "Enable/Disable Auto Export" button at the top right. To set this
as the default for your project edit the file
`./bootstrap/configuration/src/main/resources/configuration/modules/autoexport-module.xml`