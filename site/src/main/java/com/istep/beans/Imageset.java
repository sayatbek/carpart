package com.istep.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;

@HippoEssentialsGenerated(internalName = "carpart:imageset")
@Node(jcrType = "carpart:imageset")
public class Imageset extends HippoGalleryImageSet {
    @HippoEssentialsGenerated(internalName = "carpart:small")
    public HippoGalleryImageBean getSmall() {
        return getBean("carpart:small", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:large")
    public HippoGalleryImageBean getLarge() {
        return getBean("carpart:large", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:smallsquare")
    public HippoGalleryImageBean getSmallsquare() {
        return getBean("carpart:smallsquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:mediumsquare")
    public HippoGalleryImageBean getMediumsquare() {
        return getBean("carpart:mediumsquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:largesquare")
    public HippoGalleryImageBean getLargesquare() {
        return getBean("carpart:largesquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:banner")
    public HippoGalleryImageBean getBanner() {
        return getBean("carpart:banner", HippoGalleryImageBean.class);
    }
}
