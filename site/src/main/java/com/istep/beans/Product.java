package com.istep.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import java.util.List;

@HippoEssentialsGenerated(internalName = "carpart:product")
@Node(jcrType = "carpart:product")
public class Product extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "carpart:title")
    public String getTitle() {
        return getProperty("carpart:title");
    }

    @HippoEssentialsGenerated(internalName = "carpart:price")
    public Double getPrice() {
        return getProperty("carpart:price");
    }

    @HippoEssentialsGenerated(internalName = "carpart:introduction")
    public String getIntroduction() {
        return getProperty("carpart:introduction");
    }

    @HippoEssentialsGenerated(internalName = "carpart:description")
    public HippoHtml getDescription() {
        return getHippoHtml("carpart:description");
    }

    @HippoEssentialsGenerated(internalName = "carpart:images")
    public List<Imageset> getImages() {
        return getLinkedBeans("carpart:images", Imageset.class);
    }
}
