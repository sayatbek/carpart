package com.istep.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoBean;

@HippoEssentialsGenerated(internalName = "carpart:bannerdocument")
@Node(jcrType = "carpart:bannerdocument")
public class Banner extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "carpart:title")
    public String getTitle() {
        return getProperty("carpart:title");
    }

    @HippoEssentialsGenerated(internalName = "carpart:content")
    public HippoHtml getContent() {
        return getHippoHtml("carpart:content");
    }

    @HippoEssentialsGenerated(internalName = "carpart:link")
    public HippoBean getLink() {
        return getLinkedBean("carpart:link", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "carpart:image")
    public Imageset getImage() {
        return getLinkedBean("carpart:image", Imageset.class);
    }
}
